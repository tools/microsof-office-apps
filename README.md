


### Uninstall Microsoft apps from MacOS

delete pin app from your dock and reboot.

```bash
sudo rm -rf /Applications/Microsoft\ Word.app
sudo rm -rf /Applications/Microsoft\ Excel.app
sudo rm -rf /Applications/Microsoft\ PowerPoint.app
sudo rm -rf /Applications/Microsoft\ OneNote.app
sudo rm -rf /Applications/Microsoft\ OneDrive.app
sudo rm -rf /Applications/Microsoft\ Outlook.app




sudo rm -rf ~/Library/Preferences/com.microsoft.*
sudo rm -rf ~/Library/Application\ Support/Microsoft/
sudo rm -rf ~/Library/Containers/com.microsoft.*
sudo rm -rf ~/Library/Group\ Containers/UBF8T346G9.Office

```
